sap.ui.controller("view.BarcodeScan", {

    onInit: function() {
        this.bus = sap.ui.getCore().getEventBus();
        //llenar la tabla	
        oMatID = [];
        oMatDescr = [];
    },


    onNavButtonTap: function(evt) {
        var bindingContext = evt.getSource().getBindingContext();

        var bus = sap.ui.getCore().getEventBus();
        bus.publish("nav", "to", {
            id: "homepage",
            data: {
                context: bindingContext
            }
        });
    },
    /*
     * Método encargado de scannear un QR y obtener la información para enviarla al servicio SAP
     */
    doScan: function() {

       
        cordova.plugins.barcodeScanner.scan(
            function(result) {
                //result.text - toma el resultado de la lectura del QR
                /*var result = {
                    "text" : "hola mundo"
                }*/
                if (result != undefined && result.text.length > 0) {
                   // this.updateTable();
                
                    sap.ui.core.BusyIndicator.show();
                    var sUrl = "http://sapbep.seidor.es:8010/sap/opu/odata/SEIM/SSALESODATA_SRV";
                    var oModel = new sap.ui.model.odata.ODataModel(sUrl, true, "seidor", "consultor");

                    oModel.read(
                        "/ActivitySet('100000004')",
                        null,
                        null,
                        true,
                        function(oData, response) {

                            sap.ui.getCore().byId("_inputResultQR").setValue(result.text);
                            sap.ui.getCore().byId("_txtKtext").setText(" var1: " + oData.Ktext);
                            sap.ui.getCore().byId("_txtKunnr").setText(" var2: " + oData.Kunnr);

                            oMatID.push(result.text);
                            oMatDescr.push(oData.Ktext);
                           
                            var data = [];
                            for (var i = 0, len = oMatID.length; i < len; ++i)
                                data.unshift({ "MatID": oMatID[i], "MatDescr": oMatDescr[i], "cant": "0" });

                            var oModel1 = new sap.ui.model.json.JSONModel({ "zgwmat": data });
                            sap.ui.getCore().setModel(oModel1, "zgwmatmodel");
                            sap.ui.core.BusyIndicator.hide();
                        }

                    );
                }//condición resultado scan
                else
                {
                	alert("No se obtuvo resultado del scan");
                }
            } //function result
        ); //close scan


        /*cordova.plugins.barcodeScanner.scan(
				function (result) {
					jQuery.sap.require("sap.ui.model.odata.datajs");
					var sUrl = "http://sapgatewayserver.com:8000/sap/opu/odata/sap/ZGW_MATERIAL_SRV";
					var oModel = new sap.ui.model.odata.ODataModel(sUrl, true, "username", "password");
		 
					oModel.read(
						 "/Materials('" + result.text + "')", 
						 null,
						 null,
						 true, 
						 function(oData, response) { 
							
							 sap.ui.core.BusyIndicator.hide();
							 console.log(response.data.Material);
					 
							 var oMatID = []; var oMatDescr=[];
							oMatID.push(response.data.Material);
							oMatDescr.push(response.data.MatlDesc);
									
							var data = [];
							for(var i = 0; i < oMatID.length; i++) {
								data.push({"MatID": oMatID[i], "MatDescr": oMatDescr[i]});
							}
							var oModel1 = new sap.ui.model.json.JSONModel({ "zgwmat": data });
							sap.ui.getCore().setModel(oModel1, "zgwmatmodel");
	
						
							var bus = sap.ui.getCore().getEventBus();
								bus.publish("nav", "to", { 
								id : "scanresult",
						
							});
							
						 },
						 function (err) {
			            	 alert("Error in Get -- Request " + err.response.body);
			                 console.log(err.response.body);
			            }
					);

			      }, 
			    function (error) {
			       alert("Scanning failed: " + error);
			   }
		);*/
        /*cordova.plugins.barcodeScanner.scan(
                    function (result) {
                    	alert(result.text);
                    
                    },
                    function (error) {
                        alert("Scanning failed: " + error);
                    }
        );*/

    },
    request_perm: function() {

        var that = this;
        var permissions = cordova.plugins.permissions;

        permissions.hasPermission(permissions.CAMERA, checkPermissionCallback, null);

        function checkPermissionCallback(status) {
            if (!status.hasPermission) {
                var errorCallback = function() {
                    console.warn('Camera permission is not turned on');
                }

                permissions.requestPermission(
                    permissions.CAMERA,
                    function(status) {
                        if (!status.hasPermission)
                            errorCallback();
                        else
                            that.doScan();
                    },
                    errorCallback);
            } else
                that.doScan();
            
        }
    },

    hasCameraPermission: function() {
       
        cordova.plugins.barcodeScanner.hasCameraPermission(
            function(result) {
                // if this is 'false' you probably want to call 'requestCameraPermission' now
                if (!result)
                    this.requestCameraPermission();
                else
                    this.doScan();
            }
        )
    },

    requestCameraPermission: function() {
        // no callbacks required as this opens a popup which returns async
        cordova.plugins.barcodeScanner.requestCameraPermission();
    },

    sendData: function() {
        var table = sap.ui.getCore().byId("otablematqr");
        var status = true;    
        for (var i = 0, len = table.getItems().length; i < len; ++i) {
            var cant = table.getItems()[i].getCells()[2].getValue();
            sap.ui.getCore().getModel("zgwmatmodel").oData.zgwmat[i].cant = cant;
            if(cant == "")
                status = false;
        }
        //console.log(this.getView().getModel("zgwmatmodel").oData);
        alert(status ? "Los datos han sido enviados al servidor" : "No deben haber cantidades vacias" );
        
    },

    updateTable  : function()
    {
        var table = sap.ui.getCore().byId("otablematqr");
      
        for (var i = 0, len = table.getItems().length; i < len; ++i) {
            var cant = table.getItems()[i].getCells()[2].getValue();
            sap.ui.getCore().getModel("zgwmatmodel").oData.zgwmat[i].cant = cant;
           
        }

        console.log("nuevo");
    },




});
