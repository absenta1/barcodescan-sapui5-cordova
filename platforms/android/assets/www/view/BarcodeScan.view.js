sap.ui.jsview("view.BarcodeScan", {

    getControllerName: function() {
        return "view.BarcodeScan";
    },

    createContent: function(oController) {

        var oPage = new sap.m.Page({
            title: "Barcode Scanner",
            showNavButton: true,
            navButtonTap: [oController.onNavButtonTap],
            content: [
                this.oScanPullToRefresh,
                this.oScanHeader,
                this.oScanList,
                new sap.ui.core.HTML({ content: '<br>' }),
                new sap.m.Button({
                    text: "Scan code",
                    type: sap.m.ButtonType.Accept,
                    press: function() {
                        
                        var deviceType = cordova.platformId; //nombre de la plataforma que se está ejecutando
                        if (deviceType == "android")
                            oController.request_perm();
                        else
                            oController.doScan();

                        /*switch (cordova.platformId) {
                            case "android":
                                //code block
                                 oController.request_perm();
                                break;
                            case "ios":
                                //code block
                                 oController.doScan();
                                break;
                            case "browser":
                                oController.doScan();
                                break;
                            default:
                                break;
                        }*/

                    }
                }).addStyleClass('margin'),

                new sap.ui.core.HTML({ content: '<br><br>' }),

                new sap.m.Label({
                    id: "_txtTitle",
                    text: "Datos lectura QR",
                    design: "Bold",

                }).addStyleClass('margin'),
                new sap.ui.core.HTML({ content: '<br>' }),
                new sap.m.Input({
                    id: "_inputResultQR",
                    type: "Text",
                    placeholder: "Resultado lectura del QR",
                }).addStyleClass('margin'),
                new sap.ui.core.HTML({ content: '<br><br>' }),
                new sap.m.Label({
                    id: "_txtKtext",
                    text: "text",
                }).addStyleClass('margin'),



                new sap.m.Label({
                    id: "_txtKunnr",
                    text: "kunnr",
                }).addStyleClass('margin'),
                new sap.ui.core.HTML({ content: '<br>' }),

                new sap.m.Button({
                    text: "Enviar datos",
                    type: sap.m.ButtonType.Reject,
                    press: function() { oController.sendData(); }
                }),

            ]
        });


        var oTableMatQR = new sap.m.Table("otablematqr", {

            inset: true,
            headerText: "Tabla de resultados",
            columns: [
                new sap.m.Column({ header: new sap.m.Label({ text: "QR" }), width: '30%' }),
                new sap.m.Column({ header: new sap.m.Label({ text: "Descripción" }), width: '30%' }),
                new sap.m.Column({ header: new sap.m.Label({ text: "Cantidad" }), width: '20%' }),
            ],
        });

        oTableMatQR.bindAggregation("items", {
            path: "zgwmatmodel>/zgwmat",
            template: new sap.m.ColumnListItem({
                cells: [
                    new sap.m.Label({ text: "{zgwmatmodel>MatID}" }),
                    new sap.m.Label({ text: "{zgwmatmodel>MatDescr}" }),
                    new sap.m.Input({
                        text: "{zgwmatmodel>cant}",
                        type: "Number",
                        placeholder: "0"
                    }),
                ]
            })
        });


        oPage.addContent(oTableMatQR);


        return oPage;
    },
});
